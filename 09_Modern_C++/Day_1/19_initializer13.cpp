/*  Aggregate type 
    => Brace initializer with out constructor.
    => C array, struct, etc.
*/

struct Point
{
    int x, y;

    void foo() {}   /*  When there is method in class. 
                        C++ allows aggregate type.
                    */
    // virtual void goo() {}   /*  virtual method does not allow 
    //                             aggregate type.
    //                         */
    // Point() {}   /* User defined constructor => not aggregate type. */
    Point() = default;  /* Constructor generated by compiler allows aggregate type. */
};

int main()
{
    Point p1;
    Point p2 = {1,2};
}